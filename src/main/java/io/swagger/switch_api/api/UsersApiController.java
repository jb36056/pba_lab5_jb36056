package io.swagger.switch_api.api;

import io.swagger.switch_api.model.CreateRequest;
import io.swagger.switch_api.model.Error;
import java.util.UUID;
import io.swagger.switch_api.model.UpdateRequest;
import io.swagger.switch_api.model.User;
import io.swagger.switch_api.model.UserListResponse;
import io.swagger.switch_api.model.UserResponse;

import io.swagger.switch_api.model.UserRepository;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-04-13T18:23:05.437+02:00")

@Controller
public class UsersApiController implements UsersApi {

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
        this.mapLoginPass.put("jb36056", "123456");
    }
    
    private final Map<String, String> mapLoginPass = new HashMap<String, String>();
	
    
    @Autowired
    UserRepository repository;

    public ResponseEntity<UserResponse> createUser(@ApiParam(value = "User object that has to be added" ,required=true )  @Valid @RequestBody CreateRequest body) {
    	User newUsr = body.getUser();
    	UUID reqId = body.getRequestHeader().getRequestId();
    	String json = new Gson().toJson(newUsr);
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
        String accept = request.getHeader("Accept");
    	User usr = repository.findOne(newUsr.getId());
        if (accept != null && accept.contains("application/json")) {
        	if(usr == null) {
	            try {
	            	repository.save(newUsr);
	                return new ResponseEntity<UserResponse>(objectMapper.readValue("{  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\"  },  \"user\" : "+json+" }", UserResponse.class), HttpStatus.CREATED);
	            } catch (IOException e) {
	                log.error("Couldn't serialize response for content type application/json", e);
	                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
	            }
        	}
        	else {
        		return new ResponseEntity<UserResponse>(HttpStatus.UNPROCESSABLE_ENTITY);
        	}
        }

        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }
    
    public ResponseEntity<Void> deleteUser(@ApiParam(value = "",required=true) @PathVariable("id") Long id) {
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
        String accept = request.getHeader("Accept");
    	if (accept != null && accept.contains("application/json")) {            
	    	User usr = repository.findOne(id);
	    	if(usr != null) {
	    		repository.delete(id);
	    		return new ResponseEntity<Void>(HttpStatus.OK);
	    	}
	    	else {
	    		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);    		
	    	}
    	}
        return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST); 
    }
    
    public ResponseEntity<UserListResponse> getAllUsers() {
    	String json = new Gson().toJson(repository.findAll());
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<UserListResponse>(objectMapper.readValue("{  \"usersList\" : "+json+",  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \"046b6c7f-0b8a-43b9-b35d-6489e6daee91\"  }}", UserListResponse.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<UserListResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<UserListResponse>(HttpStatus.BAD_REQUEST); 
    }

    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	User usr = repository.findOne(id);
        	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
        	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        	df.setTimeZone(tz);
        	String nowAsISO = df.format(new Date());
        	if(usr != null) {
        		try {
                	String json = new Gson().toJson(usr);
					return new ResponseEntity<UserResponse>(objectMapper.readValue("{  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \"046b6c7f-0b8a-43b9-b35d-6489e6daee91\"  },  \"user\" : "+json+" }", UserResponse.class), HttpStatus.OK);
				}  
        		catch (IOException e) {
	                log.error("Couldn't serialize response for content type application/json", e);
	                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
	            }
        	}
        	else {
        		return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);    	
        	}
        }

        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<UserResponse> updateUser(@ApiParam(value = "",required=true) @PathVariable("id") Long id,@ApiParam(value = "" ,required=true )  @Valid @RequestBody UpdateRequest body) {
    	User updatedUsr = body.getUser();
    	User usr = repository.findOne(id);
    	UUID reqId = body.getRequestHeader().getRequestId();
    	String json = new Gson().toJson(updatedUsr);
    	TimeZone tz = TimeZone.getTimeZone("GMT+2:00");
    	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    	df.setTimeZone(tz);
    	String nowAsISO = df.format(new Date());
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	if(usr != null) {
	            try {
	            	repository.save(updatedUsr);
	                return new ResponseEntity<UserResponse>(objectMapper.readValue("{  \"responseHeader\" : {    \"sendDate\" : \""+nowAsISO+"\",    \"requestId\" : \""+reqId+"\"  },  \"user\" : "+json+" }", UserResponse.class), HttpStatus.OK);
	            } catch (IOException e) {
	                log.error("Couldn't serialize response for content type application/json", e);
	                return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
	            }
        	}
        	else {
        		return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);    	
        	}
        }

        return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);
    }
    
    private class Login {
    	String name;
    	String pass;
    	
    	private Login(String name, String pass) {
    		this.name = name;
    		this.pass = pass;
    	}
    }

}
